const allPictures = document.querySelectorAll('.image-to-show');
const wrapper = document.querySelector('.images-wrapper');
const btnStop = document.getElementById('btn-stop');
const btnStart = document.getElementById('btn-start');

function showPictures (){
    btnStop.style.opacity = '1';
    allPictures.forEach(element => {
        element.classList.remove('active');
    })
    const visiblePicture = document.querySelector('.image-to-show.active');
    if(visiblePicture.nextElementSibling === null){
        wrapper.firstElementChild.classList.add('active')
    } else {
        visiblePicture.nextElementSibling.classList.add('active');
    }
}

let startTimer = setInterval(() => showPictures(), 3000);

btnStop.addEventListener("click", ()=> {
    btnStart.style.opacity = "1";
    clearInterval(startTimer); 
})
btnStart.addEventListener("click", () => {
   startTimer = setInterval(() => showPictures(), 3000);
})
